package golangsdcsdk

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Sdk struct {
	username string
	pass     string
	token    string
	basename string
}

// todo questa deve essere un costruttore che restituisce un sdk per chiamate autenticate o anonime
func (s *Sdk) Init(user string, psw string, tenant string) error {
	//Todo se username e psw sono nulle , abbiamo un utente anonimo. Aggiungere controllo
	token, err := getToken(user, psw, tenant)
	if err != nil {
		return err
	}
	s.username = user
	s.pass = psw
	s.token = token
	s.basename = buildBaseUrl(tenant)
	return nil
}
func getToken(user, pass, baseurl string) (string, error) {
	// Construct the URL for authentication.
	authURL := buildBaseUrl(baseurl) + "/auth"

	// Define the request body.
	reqBody := map[string]string{
		"username": user,
		"password": pass,
	}
	reqBodyBytes, err := json.Marshal(reqBody)
	if err != nil {
		return "", fmt.Errorf("error marshaling JSON: %w", err)
	}

	// Send the authentication request.
	resp, err := http.Post(authURL, "application/json", bytes.NewReader(reqBodyBytes))
	if err != nil {
		return "", fmt.Errorf("error sending HTTP request: %w", err)
	}
	defer resp.Body.Close()

	// Read the response body.
	respBodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("error reading response body: %w", err)
	}

	// Parse the token from the response body.
	var tokenResp struct {
		Token string `json:"token"`
	}
	if err := json.Unmarshal(respBodyBytes, &tokenResp); err != nil {
		return "", fmt.Errorf("error parsing token response: %w", err)
	}

	token := tokenResp.Token
	fmt.Println("Token:", token)
	return token, nil
}
func buildBaseUrl(tenant string) string {
	baseurl := "https://qa.stanzadelcittadino.it/" + tenant + "/api"
	return baseurl
}
